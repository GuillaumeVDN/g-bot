from discord.ext import tasks
from ticket.ticket import get_ticket
from ticket.ticket_category import TicketCategory


def setup_auto_close_triage_tickets():
    @tasks.loop(seconds=30)
    async def auto_close():
        for category_channel in TicketCategory.TRIAGE.categories:
            for channel in category_channel.text_channels:
                ticket = get_ticket(channel)
                if ticket is not None:
                    await ticket.auto_close_or_notify()

    auto_close.start()
