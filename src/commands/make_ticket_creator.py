import discord
from commands.checks.check_is_staff import is_staff
from plugin.plugin import Plugin
from state import state


def setup_make_ticket_creator_command():
    @state.client.tree.command(
        description="Create a help ticket creator",
    )
    @is_staff()
    async def make_ticket_creator(interaction: discord.Interaction, plugin: Plugin):
        await interaction.response.defer(ephemeral=True, thinking=True)

        await interaction.channel.send(
            embed=discord.Embed(
                description=f"""
                Click the button below to create a help ticket for {plugin.display_name}.

                If you have multiple issues, please create a separate ticket for each one!
                """,
                color=16711922,
            ),
            view=discord.ui.View().add_item(
                discord.ui.Button(
                    label=f"{plugin.display_name} ticket",
                    emoji=plugin.emoji,
                    style=discord.ButtonStyle.primary,
                    custom_id=f"create_ticket_{plugin.id}",
                )
            ),
        )
        await interaction.followup.send("Ticket creator was created.", ephemeral=True)
