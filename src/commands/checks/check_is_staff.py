import discord
from state import state
from discord.app_commands import check


def is_staff():
    def predicate(interaction: discord.Interaction) -> bool:
        return any(role in interaction.user.roles for role in state.staff_roles)

    return check(predicate)
