import discord
from commands.checks.check_is_staff import is_staff
from state import state
from ticket.ticket import get_ticket
from ticket.ticket_category import TicketCategory


def setup_category_commands():
    async def category_command(
        interaction: discord.Interaction, category: TicketCategory
    ):
        await interaction.response.defer(thinking=True)

        ticket = get_ticket(interaction.channel)
        if ticket is None:
            return await interaction.followup.send(
                "This is not a ticket channel.", ephemeral=True
            )

        await ticket.move(category, interaction=interaction)

    @state.client.tree.command(description="Move a ticket to a category")
    @is_staff()
    async def move(interaction: discord.Interaction, category: TicketCategory):
        await category_command(interaction, category)

    @state.client.tree.command(description="Move a ticket to triage")
    @is_staff()
    async def triage(interaction: discord.Interaction):
        await category_command(interaction, TicketCategory.TRIAGE)

    @state.client.tree.command(description="Move a ticket to high priority")
    @is_staff()
    async def high(interaction: discord.Interaction):
        await category_command(interaction, TicketCategory.HIGH_PRIORITY)

    @state.client.tree.command(description="Move a ticket to medium priority")
    @is_staff()
    async def medium(interaction: discord.Interaction):
        await category_command(interaction, TicketCategory.MEDIUM_PRIORITY)

    @state.client.tree.command(description="Move a ticket to low priority")
    @is_staff()
    async def low(interaction: discord.Interaction):
        await category_command(interaction, TicketCategory.LOW_PRIORITY)

    @state.client.tree.command(description="Close a ticket")
    async def close(interaction: discord.Interaction):
        await category_command(interaction, TicketCategory.CLOSED)
