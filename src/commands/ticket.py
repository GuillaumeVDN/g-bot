import discord
from commands.checks.check_is_staff import is_staff
from plugin.plugin import Plugin
from state import state
from ticket.ticket import create_ticket_from_interaction


def setup_ticket_command():
    @state.client.tree.command(
        description="Create a help ticket",
    )
    @is_staff()
    async def ticket(
        interaction: discord.Interaction, author: discord.User, plugin: Plugin
    ):
        await interaction.response.defer(ephemeral=True, thinking=True)

        await create_ticket_from_interaction(interaction, author, plugin)
