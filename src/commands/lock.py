import discord
from commands.checks.check_is_staff import is_staff
from state import state
from ticket.ticket import get_ticket


def setup_lock_commands():
    @state.client.tree.command(description="Lock a ticket to disable auto-close")
    @is_staff()
    async def lock(interaction: discord.Interaction):
        await interaction.response.defer(ephemeral=True, thinking=True)

        ticket = get_ticket(interaction.channel)
        if ticket is None:
            return await interaction.followup.send(
                "This is not a ticket channel.", ephemeral=True
            )

        if ticket.is_auto_close_locked():
            return await interaction.followup.send(
                "This ticket is already locked.", ephemeral=True
            )

        await ticket.set_auto_close_locked(True)
        await interaction.followup.send("Ticket locked.", ephemeral=True)

    @state.client.tree.command(description="Un-lock a ticket to enable auto-close")
    @is_staff()
    async def unlock(interaction: discord.Interaction):
        await interaction.response.defer(ephemeral=True, thinking=True)

        ticket = get_ticket(interaction.channel)
        if ticket is None:
            return await interaction.followup.send(
                "This is not a ticket channel.", ephemeral=True
            )

        if not ticket.is_auto_close_locked():
            return await interaction.followup.send(
                "This ticket is not locked.", ephemeral=True
            )

        await ticket.set_auto_close_locked(False)
        await interaction.followup.send("Ticket unlocked.", ephemeral=True)
