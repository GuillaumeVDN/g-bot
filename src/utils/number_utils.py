def int_or_none(value: str) -> int | None:
    try:
        return int(value)
    except ValueError:
        return None
