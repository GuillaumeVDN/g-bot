import discord


class Client(discord.Client):
    def __init__(self, *, intents: discord.Intents):
        super().__init__(intents=intents)
        self.tree = discord.app_commands.CommandTree(self)

    async def setup_hook(self):
        from state import state

        self.tree.copy_global_to(guild=state.guild)
        await self.tree.sync(guild=state.guild)
