import textwrap
import discord
from datetime import datetime, timedelta, timezone
from plugin.plugin import Plugin
from state import state
from ticket.ticket_category import TicketCategory
from utils.number_utils import int_or_none


AUTO_CLOSE_DELAY_NOTIFY = timedelta(days=2)
AUTO_CLOSE_DELAY = timedelta(days=14)


class Ticket:
    def __init__(
        self,
        id: int,
        plugin: Plugin,
        author: discord.User | None,
        channel: discord.TextChannel,
    ):
        self.id = id
        self.plugin = plugin
        self.author = author
        self.channel = channel

    def get_category(self) -> TicketCategory | None:
        for category in TicketCategory:
            if self.channel.category in category.categories:
                return category
        return None

    def is_auto_close_locked(self) -> bool:
        return self.channel.topic == "Auto-close is disabled."

    async def set_auto_close_locked(self, locked: bool):
        await self.channel.edit(topic="Auto-close is disabled." if locked else None)

    def get_auto_close_date(self) -> datetime | None:
        if self.channel.topic is not None and self.channel.topic.startswith(
            "This ticket will auto-close on "
        ):
            return datetime.fromisoformat(self.channel.topic[31:41]).replace(
                tzinfo=timezone.utc
            )
        return None

    async def set_auto_close_date(self, deadline: datetime | None):
        if deadline is None:
            await self.channel.edit(topic=None)
        else:
            await self.channel.edit(
                topic=f"This ticket will auto-close on {deadline.isoformat()[:10]} (UTC) without a reply."
            )

    async def auto_close_or_notify(self):
        if self.is_auto_close_locked():
            return

        now = datetime.now(timezone.utc)
        auto_close_date = self.get_auto_close_date()

        last_message = None
        async for message in self.channel.history(limit=25):
            if not message.author.bot:
                last_message = message
                break
        if last_message is None:
            return
        last_message_is_staff = any(
            role in state.staff_roles for role in last_message.author.roles
        )

        # has deadline
        if auto_close_date is not None:
            # author replied : remove deadline
            if not last_message_is_staff:
                await self.set_auto_close_date(None)
            # no reply and passed date : close self
            elif now > auto_close_date:
                await self.move(TicketCategory.CLOSED, auto_close=True)
        # should have deadline
        elif (
            last_message_is_staff
            and now > last_message.created_at + AUTO_CLOSE_DELAY_NOTIFY
        ):
            await self.set_auto_close_date(now + AUTO_CLOSE_DELAY)

            await self.channel.send(
                embed=discord.Embed(
                    description=f"{self.author.mention + ' ' if self.author is not None else ''}It seems more information is needed for this ticket.",
                    color=16711922,
                ).set_footer(
                    text=f"Ticket will auto-close on {(now + AUTO_CLOSE_DELAY).isoformat()[:10]} (UTC) without a reply."
                )
            )

            if self.author is not None:
                ping = await self.channel.send(f"{self.author.mention}")
                await ping.delete()

    async def move(
        self,
        category: TicketCategory,
        interaction: discord.Interaction | None = None,
        auto_close: bool = False,
    ):
        previous_category = self.get_category()
        if category == previous_category:
            if interaction is not None:
                await interaction.followup.send(
                    "Ticket is already in this category.", ephemeral=True
                )
            return

        # clear reply deadline
        await self.set_auto_close_date(None)

        # move ticket
        category_channel = await category.get_free_category(
            delete_old=category is TicketCategory.CLOSED
        )
        await self.channel.move(category=category_channel, end=True)

        # update permissions (reopening/closing)
        if self.author is not None:
            if category is TicketCategory.CLOSED:
                await self.channel.set_permissions(
                    self.author, view_channel=True, send_messages=False
                )
            elif previous_category is TicketCategory.CLOSED:
                await self.channel.set_permissions(
                    self.author, view_channel=True, send_messages=True
                )

        # send confirmation
        embed = discord.Embed(
            description=(
                "Ticket was reopened."
                if previous_category is TicketCategory.CLOSED
                else (
                    (
                        "Ticket was automatically closed due to inactivity."
                        if auto_close
                        else "Ticket was closed."
                    )
                    if category is TicketCategory.CLOSED
                    else f"Ticket was moved to '{category.display_name.lower()}'."
                )
            ),
            color=16711922,
        )
        view = (
            discord.ui.View().add_item(
                discord.ui.Button(
                    label="Reopen ticket",
                    emoji="🔒",
                    style=discord.ButtonStyle.secondary,
                    custom_id="reopen_ticket",
                )
            )
            if category is TicketCategory.CLOSED
            else discord.utils.MISSING
        )

        if interaction is not None:
            await interaction.followup.send(
                embed=embed,
                view=view,
                ephemeral=False,
            )
        else:
            await self.channel.send(embed=embed, view=view)


def get_ticket(channel: discord.TextChannel) -> Ticket | None:
    parts = channel.name.split("-", 3)
    if len(parts) < 3:
        return None

    id = int_or_none(parts[0])
    if id is None:
        return None

    parts[1] = parts[1].lower()
    plugin = next(plugin for plugin in Plugin if parts[1] == plugin.id)
    if plugin is None:
        return None

    potential_authors = [
        member for member in channel.overwrites if isinstance(member, discord.Member)
    ]
    author = next(
        (
            member
            for member in potential_authors
            if all(role not in state.staff_roles for role in member.roles)
        ),
        potential_authors[0] if potential_authors else None,
    )

    return Ticket(id, plugin, author, channel)


async def create_ticket(author: discord.User, plugin: Plugin) -> Ticket:
    # find highest ticket number across all categories
    highest_ticket_id = 0
    for category in TicketCategory:
        for category in category.categories:
            for channel in category.text_channels:
                ticket = get_ticket(channel)
                if ticket is not None and ticket.id > highest_ticket_id:
                    highest_ticket_id = ticket.id

    # create ticket channel
    triage_category = await TicketCategory.TRIAGE.get_free_category()

    ticket_id = highest_ticket_id + 1
    ticket_name = f"{ticket_id}-{plugin.id}-{author.name}"
    ticket_channel = await state.guild.create_text_channel(
        ticket_name, category=triage_category
    )

    # restrict view to author and staff roles
    await ticket_channel.set_permissions(
        state.guild.default_role, view_channel=False, send_messages=False
    )
    await ticket_channel.set_permissions(author, view_channel=True, send_messages=True)
    for role in state.staff_roles:
        await ticket_channel.set_permissions(
            role, view_channel=True, send_messages=True
        )

    # send embed
    await ticket_channel.send(
        embed=discord.Embed(
            title=f"Ticket #{ticket_id} - {plugin.display_name}",
            description=textwrap.dedent("""
                Hi, thanks for reaching out!

                Please **describe your issue** with **as much detail as possible**, such as:
                - server & plugin version ;
                - server logs ;
                - configuration files
                - steps to reproduce ;
                - screenshots ;
                - etc.

                If you have multiple issues, please create a new ticket for each one.
                """),
            color=16711922,
        ),
        view=discord.ui.View().add_item(
            discord.ui.Button(
                label="Close ticket",
                emoji="🔒",
                style=discord.ButtonStyle.secondary,
                custom_id="close_ticket",
            )
        ),
    )

    # ping author
    ping = await ticket_channel.send(f"{author.mention}")
    await ping.delete()

    return Ticket(ticket_id, plugin, author, ticket_channel)


async def create_ticket_from_interaction(
    interaction: discord.Interaction, author: discord.User, plugin: Plugin
):
    ticket = await create_ticket(author, plugin)
    await interaction.followup.send(
        embed=discord.Embed(
            title=f"Ticket #{ticket.id} - {ticket.plugin.display_name}",
            description=f"Ticket was created in {ticket.channel.mention}.",
            color=16711922,
        ),
        ephemeral=True,
    )
