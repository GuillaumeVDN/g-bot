from enum import Enum
from os import environ

import discord
from state import state


class TicketCategory(Enum):
    TRIAGE = "Triage"
    HIGH_PRIORITY = "High priority"
    MEDIUM_PRIORITY = "Medium priority"
    LOW_PRIORITY = "Low priority"
    CLOSED = "Closed"

    def __init__(self, display_name: str):
        self.display_name: str = display_name
        self.categories: list[discord.CategoryChannel] = []

    async def get_free_category(
        self, delete_old: bool = False
    ) -> discord.CategoryChannel:
        try:
            return next(
                category
                for category in self.categories
                if len(category.text_channels) < 50
            )
        except StopIteration:
            if delete_old:
                # delete oldest ticket in triage category
                oldest_channel = None
                oldest_channel_category = None
                oldest_channel_date = None
                for category in self.categories:
                    for channel in category.text_channels:
                        # find last message in channel (oldest)
                        last_message = None
                        async for message in channel.history(limit=1):
                            last_message = message
                            if last_message is None:
                                continue
                            if oldest_channel is None:
                                oldest_channel = channel
                                oldest_channel_category = category
                                oldest_channel_date = last_message.created_at
                            elif last_message.created_at < oldest_channel_date:
                                oldest_channel = channel
                                oldest_channel_category = category
                                oldest_channel_date = last_message.created_at

                if oldest_channel is not None:
                    await oldest_channel.delete()
                    return oldest_channel_category

            raise Exception("No room for new ticket in triage category")


def init_ticket_categories():
    for category in TicketCategory:
        category.categories = [
            state.guild.get_channel(int(category_id))
            for category_id in environ.get(
                f"G_BOT_TICKET_CATEGORIES_{category.name}_IDS"
            ).split(",")
        ]
