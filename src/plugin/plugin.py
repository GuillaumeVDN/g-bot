from enum import Enum


class Plugin(Enum):
    QUESTCREATOR = ("QuestCreator", "🏹")
    BETTINGGAMES = ("BettingGames", "💸")
    POTATOES = ("Potatoes", "🥔")
    CUSTOMCOMMANDS = ("CustomCommands", "⌨")
    GPARTICLES = ("GParticles", "✨")
    GSLOTMACHINE = ("GSlotMachine", "🎰")

    def __init__(self, display_name: str, emoji: str):
        self.id: str = self.name.lower()
        self.display_name: str = display_name
        self.emoji: str = emoji

    def __str__(self):
        return self.display_name
