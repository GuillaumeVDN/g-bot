from client import Client
from commands.category import setup_category_commands
from commands.make_ticket_creator import setup_make_ticket_creator_command
from commands.lock import setup_lock_commands
from commands.ticket import setup_ticket_command
import discord
from discord_sentry_reporting import use_sentry
from dotenv import find_dotenv, load_dotenv
from events.on_app_command_error import setup_on_app_command_error
from events.on_interaction import setup_on_interaction
from os import environ
from state import state
from tasks.auto_close_tickets import setup_auto_close_triage_tickets
from ticket.ticket_category import init_ticket_categories


# load environment variables
load_dotenv(find_dotenv())

# setup client
intents = discord.Intents.default()
intents.members = True
intents.message_content = True
state.guild = discord.Object(id=environ.get("G_BOT_GUILD_ID"))
state.client = Client(intents=intents)

use_sentry(
    state.client,
    dsn="https://4485d65449e796bcfb66001eefc24214@o4507160065343488.ingest.de.sentry.io/4507160067899472",
)

# setup commands
setup_make_ticket_creator_command()
setup_ticket_command()
setup_category_commands()
setup_lock_commands()

# setup events
setup_on_app_command_error()
setup_on_interaction()


@state.client.event
async def on_ready():
    # setup state
    state.guild = state.client.get_guild(int(environ.get("G_BOT_GUILD_ID")))
    state.staff_roles = [
        state.guild.get_role(int(role_id))
        for role_id in environ.get("G_BOT_STAFF_ROLES_IDS").split(",")
    ]
    init_ticket_categories()

    # setup tasks
    setup_auto_close_triage_tickets()

    print(f"Bot is ready as {state.client.user} ({state.client.user.id})")


# run bot
state.client.run(environ.get("G_BOT_TOKEN"))
