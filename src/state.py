from client import Client
import discord


class State:
    def __init__(self):
        self.client: Client = None
        self.guild: discord.Guild = None
        self.staff_roles: list[discord.Role] = None


state = State()
