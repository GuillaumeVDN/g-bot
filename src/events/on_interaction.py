import discord
from plugin.plugin import Plugin
from state import state
from ticket.ticket import create_ticket_from_interaction, get_ticket
from ticket.ticket_category import TicketCategory


def setup_on_interaction():
    @state.client.event
    async def on_interaction(interaction: discord.Interaction):
        if interaction.type == discord.InteractionType.component:
            if interaction.data["component_type"] == discord.ComponentType.button.value:
                custom_id = interaction.data["custom_id"]

                # create ticket button
                if custom_id.startswith("create_ticket_"):
                    await interaction.response.defer(ephemeral=True, thinking=True)

                    plugin = Plugin[custom_id[14:].upper()]
                    await create_ticket_from_interaction(
                        interaction, interaction.user, plugin
                    )

                # close ticket button
                elif custom_id == "close_ticket":
                    await interaction.response.defer(thinking=True)

                    ticket = get_ticket(interaction.channel)
                    if ticket is None:
                        await interaction.followup.send(
                            "This is not a ticket channel.", ephemeral=True
                        )
                    elif ticket.get_category() is TicketCategory.CLOSED:
                        await interaction.followup.send(
                            "This ticket is already closed.", ephemeral=True
                        )
                    else:
                        await ticket.move(
                            TicketCategory.CLOSED, interaction=interaction
                        )

                # reopen ticket button
                elif custom_id == "reopen_ticket":
                    await interaction.response.defer(thinking=True)

                    ticket = get_ticket(interaction.channel)
                    if ticket is None:
                        return await interaction.followup.send(
                            "This is not a ticket channel.", ephemeral=True
                        )
                    elif ticket.get_category() is not TicketCategory.CLOSED:
                        await interaction.followup.send(
                            "This ticket is not closed.", ephemeral=True
                        )
                    else:
                        await ticket.move(
                            TicketCategory.TRIAGE, interaction=interaction
                        )
