import discord
from state import state


def setup_on_app_command_error():
    @state.client.tree.error
    async def on_app_command_error(
        interaction: discord.Interaction, error: discord.app_commands.AppCommandError
    ):
        if isinstance(error, discord.app_commands.CheckFailure):
            await interaction.followup.send(
                "You cannot use this command.", ephemeral=True
            )
